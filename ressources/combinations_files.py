maxB=50
maxC=10
maxcombinations=19068840

maxcombinations_perfile=5000
number_file=0
cpt_file=0

cpt = 0

fichier = open("src/assets/combinations/combinations"+str(number_file)+".json", "w")
fichier.write('{\n    "combinations" : [\n')
for b1 in range(1,maxB):
    for b2 in range(1,maxB):
        if(b2>b1):
            for b3 in range(1,maxB):
                if(b3>b2):
                    for b4 in range(1,maxB):
                        if(b4>b3):
                            for b5 in range(1,maxB):
                                if(b5>b4):
                                    for c in range(1,maxC+1):
                                        cpt+=1
                                        cpt_file+=1
                                        if(cpt%1000000==0):
                                            print("Combinations generated : "+str(cpt))
                                        fichier.write('         "'+str(b1)+"-"+str(b2)+"-"+str(b3)+"-"+str(b4)+"-"+str(b5)+"+"+str(c)+'"')
                                        if(cpt==maxcombinations or cpt_file==maxcombinations_perfile-1):
                                            fichier.write('\n    ]\n}')
                                            fichier.close()

                                            if(cpt!=maxcombinations):
                                                cpt_file = 0
                                                number_file+=1
                                                fichier = open("src/assets/combinations/combinations"+str(number_file)+".json", "w")
                                                fichier.write('{\n    "combinations" : [\n')
                                        else:
                                            fichier.write(",\n")

print("Combinations generated : "+str(cpt))