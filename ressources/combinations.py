maxB=50
maxC=10
maxcombinations=19068840

maxcombinationsperchance=1906884

fichier = open("src/assets/combinations.json", "w")
fichier.write('{\n    "combinations" : {\n')

cpt = 0

for c in range(1,maxC+1):
    cptchance = 0
    fichier.write('        "'+str(c)+'": [\n')
    for b1 in range(1,maxB):
        for b2 in range(1,maxB):
            if(b2>b1):
                for b3 in range(1,maxB):
                    if(b3>b2):
                        for b4 in range(1,maxB):
                            if(b4>b3):
                                for b5 in range(1,maxB):
                                    if(b5>b4):
                                        cpt+=1
                                        cptchance+=1
                                        fichier.write('            "'+str(b1)+"-"+str(b2)+"-"+str(b3)+"-"+str(b4)+"-"+str(b5)+"+"+str(c)+'"'+("" if cpt==maxcombinationsperchance else ",")+"\n")
                                        if(cpt%1000000==0):
                                            print("Combinations generated : "+str(cpt))
    fichier.write('        ]("" if c==(maxC+1) else ",")\n')


fichier.write('    }\n}')
print("Combinations generated : "+str(cpt))
fichier.close()