# Loto results analysis

Analysis of the lottery results provided by the [FDJ](https://www.fdj.fr/jeux-de-tirage/loto/resultats) since 2017.
Application available [here](https://loto-results-analysis.herokuapp.com/)

## Authors

*  **Thibaud Avril-Terrones** - *Initial work*

## Getting started

Loto results analysis is an app built on Angular using a Firebase database.

Built on :
-  [Angular](https://angular.io)
-  [Firebase](https://firebase.google.com/)

Interface built with [Angular Material UI](https://material.angular.io/)
Deloyed with [Heroku](https://www.heroku.com)

### Launch

```bash
ng serve
```