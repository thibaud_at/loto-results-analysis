const express = require('express');
const app = express();

app.use(express.static('./dist/loto-results-analysis'));

app.get('/*', function(req, res) {
    res.sendFile('index.html', {root: 'dist/loto-results-analysis/'});
});

const port = process.env.PORT || 4200;

app.listen(port, (err, res) => {
  if (!err) {
    console.log(`service is running on ${port}`);
  }
});