import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';

import {MatSort, Sort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

import firebase from "firebase/app";
import "firebase/database";

// import combinations_json from '../assets/combinations.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  database: any;
  data: any;
  getdata: boolean = false;
  calculInProgress: boolean = false;

  filesToUpload: any = null;
  inProgress: boolean = false;
  progress: number = 0;
  nbTirages: number = 0;
  tiragesImported: number = 0;
  deleted: boolean = false;

  boules: any[] = [];
  chance: any[] = [];
  maxC: any = null;
  minC: any = null;
  maxB1: any = null; maxB2: any = null; maxB3: any = null; maxB4: any = null; maxB5: any = null;
  minB1: any = null; minB2: any = null; minB3: any = null; minB4: any = null; minB5: any = null;

  calculdone: boolean = false;
  grid: any[] = [];
  grid_small: any[] = [];
  displayedmore: boolean = false;

  numbersB : any[] = [];
  numbersC : any[] = [];
  selected_numbers : any[] = [];
  selected_chance : any = null;
  prediction : any = null;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(){
    const firebaseConfig = {
      apiKey: "AIzaSyAA8qT1o3Wz2gH_R3E1KPWqfivJRG4kZ9U",
      authDomain: "loto-59f7d.firebaseapp.com",
      databaseURL: "https://loto-59f7d-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "loto-59f7d",
      storageBucket: "loto-59f7d.appspot.com",
      messagingSenderId: "96870500420",
      appId: "1:96870500420:web:c568641918697890a01411"
    };

    firebase.initializeApp(firebaseConfig);
    this.database = firebase.database();

    this.db_read();

    for(let i=1; i<50; i++){this.numbersB.push(i);};
    for(let i=1; i<=10; i++){this.numbersC.push(i);};
  }

  ngAfterViewInit() {
  }

  onTabChanged($event: any) {
    let clickedIndex = $event.index;
  }

  handleFileInput($event: any) {
    this.deleted = false;
    this.filesToUpload = $event.target.files;
  }

  readFiles(): void {
    if(this.filesToUpload){
      this.inProgress = true;
      this.progress = 0;
      this.nbTirages = 0;
      this.tiragesImported = 0;
  
      let files: any[] = [];
      for(let i=0; i<this.filesToUpload.length; i++){
        var file: File = this.filesToUpload.item(i);
        files.push(file)
      }
      const promises: Promise<any>[] = files.map(file => {
        return new Promise<any>((resolve, reject) => {
          this.parseFile(file).then(results => {
            resolve({
                results: results
            });
          })
        });
      });
  
      Promise.all(promises).then(values => {
        const nb_tirages = values.reduce((sum, t) => {
          return sum + t.results.length;
        }, 0);
  
        this.inProgress = false;
        this.progress = 100;
        this.db_read();
        console.log("SUCCESS IMPORT | "+nb_tirages);
      });
    }
  }

  parseFile(file: any): Promise<any>{
    const g = "combinaison_gagnante_en_ordre_croissant";
    const r1 = "rapport_du_rang1";
    const r2 = "rapport_du_rang2";
    const r3 = "rapport_du_rang3";
    const r4 = "rapport_du_rang4";
    const r5 = "rapport_du_rang5";
    const r6 = "rapport_du_rang6";
    const r7 = "rapport_du_rang7";
    const r8 = "rapport_du_rang8";
    const r9 = "rapport_du_rang9";

    return new Promise((resolve, reject) => {
      var myReader: FileReader = new FileReader();
      myReader.onloadend = (e) => {
        if(myReader.result){
          let tirages = (myReader.result as string).split("\n");

          let index_result = 10;
          let index_r1 = 12;
          let index_r2 = 14;
          let index_r3 = 16;
          let index_r4 = 18;
          let index_r5 = 20;
          let index_r6 = 22;
          let index_r7 = 24;
          let index_r8 = 26;
          let index_r9 = 28;

          tirages[0].split(";").forEach((v, index) => { if(v==g)index_result=index; if(v==r1)index_r1=index; if(v==r2)index_r2=index; if(v==r3)index_r3=index; if(v==r4)index_r4=index; if(v==r5)index_r5=index; if(v==r6)index_r6=index; if(v==r7)index_r7=index; if(v==r8)index_r8=index; if(v==r9)index_r9=index; });
          tirages.shift();
          
          this.nbTirages+=tirages.length;

          let results = tirages
            .map(t => { return t.split(";"); })
            .map(ts => { return JSON.parse(JSON.stringify({"id": ts[0], "res": { "g": ts[index_result], "r1": ts[index_r1], "r2": ts[index_r2], "r3": ts[index_r3], "r4": ts[index_r4], "r5": ts[index_r5], "r6": ts[index_r6], "r7": ts[index_r7], "r8": ts[index_r8], "r9": ts[index_r9] }})); })

          const promises: Promise<any>[] = results.map(r => {
            return new Promise<any>((resolve, reject) => {
              if(r.id && r.res){
                const ref = firebase.database().ref('results/' + r.id)
                ref.set(r.res).then(() => {
                  resolve({});
                  this.tiragesImported++;
                  this.progress = Math.floor((this.tiragesImported/this.nbTirages)*100);
                },
                (error:any) => {
                  console.error(error);
                  reject(error);
                })
              }
              else{
                resolve({});
              }
            });
          });
      
          Promise.all(promises).then(values => {
            resolve(results);
          });
        }
        else{
          reject();
        }
      };
      myReader.readAsText(file);
    });
  }

  calcul() {
    if (window.confirm("Are you sure ?")) {
      this.calculInProgress = true;
      
      console.log("TEST COMBINATIONS | "+(new Date(Date.now()).getHours()+":"+new Date(Date.now()).getMinutes()))
      const promises:Promise<any>[] = this.test();

      Promise.all(promises).then(values => {
        console.log("COMBINATIONS TEST DONE | "+values.length+" combinations tested | "+(new Date(Date.now()).getHours()+":"+new Date(Date.now()).getMinutes()))
        this.grid = values;
        this.grid.sort((a,b) => b.price - a.price)
        this.grid_small = this.grid.slice(0,10);    
        this.calculdone = true;
        this.calculInProgress = false;
      });
    }
  }

  test() : Promise<any>[] {
    let promises = [];

    const maxB=50, maxC=10;
    let cpt = 0;

    for(let c=1; c<=maxC; c++){
      for(let b1=1; b1<maxB; b1++){
        for(let b2=1; b2<maxB; b2++){
          if(b2>b1){
            for(let b3=1; b3<maxB; b3++){
              if(b3>b2){
                for(let b4=1; b4<maxB; b4++){
                  if(b4>b3){
                    for(let b5=1; b5<maxB; b5++){
                      if(b5>b4){
                        cpt++;
                        promises.push(
                          this.calcul_average([b1,b2,b3,b4,b5], c, cpt)
                        )
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return promises;
  }

  displaymore() {
    this.displayedmore = true;
  }

  calcul_average(numbers:number[], chance: number, cpt:any=null) : Promise<any> {
    const it = (x:number[], y:number[]) => {let c=0, o=0; x.forEach((i) => { i>o && y.indexOf(i) >= 0 && c++, o=i; }); return c};
    
    return new Promise<any>((resolve, reject) => {
      let price = 0;

      let nb_t=0;
      this.data.forEach((t : any) => {
        nb_t++;
        let isC = (t.val().g.split("+")[1] == chance);

        const t_numbers_array=(t.val().g.split("+")[0].split("-")).map((n: string) => parseInt(n));
        let n = it(numbers,t_numbers_array);

        switch(n){
          case 0:
            price += isC ? parseFloat((t.val().r9).replace(',','.')) : 0;
            break;
          case 1:
            price += isC ? parseFloat((t.val().r9).replace(',','.')) : 0;
            break;
          case 2:
            price += isC ? parseFloat((t.val().r7).replace(',','.')) : parseFloat((t.val().r8).replace(',','.'));
            break;
          case 3:
            price += isC ? parseFloat((t.val().r5).replace(',','.')) : parseFloat((t.val().r6).replace(',','.'));
            break;
          case 4:
            price += isC ? parseFloat((t.val().r3).replace(',','.')) : parseFloat((t.val().r4).replace(',','.'));
            break;
          case 5:
            price += isC ? parseFloat((t.val().r1).replace(',','.')) : parseFloat((t.val().r2).replace(',','.'));
            break;
        }
      });

      if(cpt!=null && cpt%1000000==0)console.log("--> "+cpt+" combinations tested");
      resolve({
        "numbers": numbers.join("-"),
        "chance": chance,
        "price": Math.round(((price/nb_t)-2.2)*100)/100
      });
    });
  }

  predict() : any {
    this.numbersB = Array(49).fill(1).map((x,i)=>i+1);
    this.numbersC = Array(10).fill(1).map((x,i)=>i+1);
  }

  calculate_prediction() : void {
    this.prediction = null;

    if(this.selected_numbers.length==5 && this.selected_chance!=null){
      this.calcul_average(this.selected_numbers.sort((a,b)=>a-b), this.selected_chance).then((result: any) => {
        this.prediction = result.price;
      })
    }
  }

  number_b(n:number) {
    this.prediction = null;
    if(this.selected_numbers.includes(n)){
      const index = this.selected_numbers.indexOf(n);
      if (index > -1) {
        this.selected_numbers.splice(index, 1);
      }
    }
    else{if(this.selected_numbers.length<5)this.selected_numbers.push(n);}
  }
  number_c(n:number) {
    this.prediction = null;
    if(this.selected_chance==n){
      this.selected_chance=null;
    }
    else{this.selected_chance = n;}
  }

  stat() : any {
    this.boules=[];
    this.chance=[];

    for(let b=1; b<50; b++){this.boules.push({"number":b,"apparition":0,"pourcentage":0});}
    for(let c=1; c<=10; c++){this.chance.push({"number":c,"apparition":0,"pourcentage":0});}

    this.data.forEach((t : any) => {
      let c = t.val().g.split("+")[1];
      this.chance.forEach(n => {
        if(n.number==c){
          n.apparition++;
          n.pourcentage=(n.apparition/this.data.numChildren())*100;
        }
      })
      t.val().g.split("+")[0].split("-").forEach((b:string) => {
        this.boules.forEach(n => {
          if(n.number==b){
            n.apparition++;
            n.pourcentage=(n.apparition/this.data.numChildren())*100;
          }
        })
      });
    })

    this.minC = this.chance.sort((a,b) => a.pourcentage - b.pourcentage)[0].number;
    this.maxC = this.chance.sort((a,b) => b.pourcentage - a.pourcentage)[0].number;
    let minBs = this.boules.sort((a,b) => a.pourcentage - b.pourcentage).slice(0, 5).map(b => b.number);
    this.minB1=minBs[0];this.minB2=minBs[1];this.minB3=minBs[2];this.minB4=minBs[3];this.minB5=minBs[4];
    let maxBs = this.boules.sort((a,b) => b.pourcentage - a.pourcentage).slice(0, 5).map(b => b.number);
    this.maxB1=maxBs[0];this.maxB2=maxBs[1];this.maxB3=maxBs[2];this.maxB4=maxBs[3];this.maxB5=maxBs[4];
  }

  db_read() : any {
    this.data = null;
    this.calculInProgress=true;

    new Promise<any>((resolve, reject) => {
      let ref_res = firebase.database().ref('results/');

      ref_res.once("value", (data:any) => {
        if(data.numChildren()!=0){
          this.data = data;
          this.getdata = true;

          this.stat();
          resolve({})
        }
        else{
          resolve({});
        }
      });
    }).then(() => {
      this.calculInProgress = false;
    })
  }

  db_delete() : any {
    if (window.confirm("Are you sure ?")) {
      firebase.database().ref('results/').set({}).then(() => {
        this.filesToUpload = null;
        this.deleted = true;
        this.data = null;
        this.calculdone = false;
        this.grid = [];
        this.getdata = false;
        this.progress = 0;
      });
    }
  }

  sortData(table: any, sort: Sort) {
    let data: any[] = [];
    switch(table){
      case 'chance':
        data = this.chance || [];
        break;
      case 'boules':
        data = this.boules || [];
        break;
    }

    if (!sort.active || sort.direction === '') {
      return;
    }

    data = data.sort((a:any, b:any) => {
      const isAsc = sort.direction === 'asc';
      return compare(a[sort.active], b[sort.active], isAsc);
    });
  }
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}